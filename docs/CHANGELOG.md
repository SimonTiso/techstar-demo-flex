# Shopify Flex theme skeleton


## 2021-02-01

### Fixed

- `gulp svg` src typo
- `gulp svg` rename output
- watch-svg added to watch-project
- config.json dependency before file exists
- process-scss error on first run for missing _src/scss/custom.scss

### Added

- Missing webpack.config.js
- `setup-folder-structure` command to create _src/scss folder structure
## 2021-01-29

### Added

- Docs/ folder with Changelog and Readme
- Empty src/ folder with blank js/index.js and scss/ folder structure
- gulpfile changes for browsersync, stylelint and Sass tieing in with Flex own gulpfile