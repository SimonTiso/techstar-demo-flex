# Flex Shopify theme skeleton

## Getting started

1. Run `npm ci`
2. Add API_PASSWORD and store URL to `.env`
3. Add API password, store URL and theme ID to config.yml
4. Run `gulp`


## Theme modifications required

1. In `layout/theme.liquid` and `layout/password.liquid` replace `{{ content_for_header }}` with
    {% if false %}
      {{ content_for_header }}
    {% endif %}
    {{ content_for_header | replace: "<body onload='document._boomrl();'>", "<bodx onload='document._boomrl();'>" }}
2. Optional: If using custom Javascript, add `<script defer src="{{ 'custom.js' | asset_url }}"></script>` to `layout/theme.liquid`
3. Optional: If using custom CSS custom properties, add `{% render 'css-variables' %}` before the stylesheet declaration in `layout/theme.liquid`

## Gulp Commands

- `gulp` Default. Start project: build JS, build Sass & combine with theme Sass. Watch Sass, JS and SVG changes. Start Themekit
- `start-project`: default task run using `gulp`
- `deploy-dev`: Deploy local changes to remote development theme id
- `download-development`: Download all files on remote development theme id
- `download-production`: Download all files on remote production theme id
- `setup-folder-structure`: Create _src folder structure fo SCSS and SVG