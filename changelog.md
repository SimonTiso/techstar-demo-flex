# Changelog

Date format is year-day-month

## 2020-01-02
- Initial setup of Flex theme, node modules and gulp finished.
- Site is synced with Shopify.