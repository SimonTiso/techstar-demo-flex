const gulp = require('gulp');

gulp.task('watch-project', gulp.series(gulp.parallel('watch-scss', 'watch-js', 'watch-svg', 'watch')));

gulp.task('build-flex', gulp.series(gulp.parallel('combine_css', 'combine_vendors_js', 'combine_utilities_js', 'currency_conversion_js', 'babel')));

gulp.task('build-project', gulp.series('convert-yml', 'build-scss', 'combine_css', 'build-js'));

gulp.task('start-project', gulp.series('build-project', gulp.parallel('watch-project', 'start-themekit')));

// download (dev)
// download-production

gulp.task('deploy-dev', gulp.series(['set-dev-env', 'build-project', 'deploy']));
//gulp.task('deploy-prod', gulp.series(['set-prod-env', 'build-project', 'deploy']));

gulp.task('default', gulp.series('start-project'));